# Tekniset perusteet

---

## Sisältö

1. HTTP-protokolla
2. Yleiskatsaus GCP-infrastruktuuriin
3. Cloudflare
4. Varnish
5. PHP
6. Drupal
7. Kehitystyö

---

## 1. HTTP-protokolla

- **Tilaton** (eng. _stateless_) tiedonsiirtoprotokolla
  - Kaikki käsittelyyn tarvittava tieto välitetään pyynnön ja vastauksen mukana
- Pyynnöt ja vastaukset ovat pelkkää tekstiä
- Useita pyyntöjä voi olla matkalla (eng. _in-flight_) samaan aikaan
- Kun asiakas on saanut vastauksen, pyyntö on täytetty ja yhteys katkaistaan

+++

## ƒ(x) = y

- Ilman ulkopuolista tilaa, HTTP-pyynnön käsittelijä toimii **deterministisesti**
  - Samanlaiseen pyyntöön saadaan aina sama vastaus

---

## 2. Yleiskatsaus GCP-infrastruktuuriin

![OpsHost GCP](https://mur.karhuhelsinki.fi/kimmo/assets/images/infra_opshost_gcp.svg)

+++

## Infrastruktuurin välimuistikerrokset

- Cloudflare
- Varnish
- OPcache

---

## 3. Cloudflare

- Hoitaa useaa roolia
  - Sisällönjakeluverkosto (CDN)
  - Nimipalvelinverkosto (DNS)
  - SSL- ja TLS-salaus

+++

Tärkein rooli:

## staattisten resurssien HTTP-välimuisti (CDN)

+++

## Staattisten resurssien HTTP-välimuisti

- Oikeassa paikassa (_downstream_)
- HTTP2-tuki automaattisesti
- Kattaa yleensä **reilusti yli 90% kaikesta sivulle ladattavasta datasta**
- Data ei kulje GCP-verkon läpi

+++

## Mirjamhelin.fi

- [Webpagetest.orgissa](https://webpagetest.org) loistava tulos
- [**PSI: 96/100**](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fmirjamhelin.fi&tab=mobile)
  - SEO-mehua ropisee!

+++

## Cloudflare: invalidointi

- Aika
- URL
- Koko välimuistin pudotus
- Drupal: JS- ja CSS-aggregointi

---

## 4. Varnish

- Dynaamisen sisällön HTTP-välimuisti
  - Estää pyyntöjä menemästä Drupalille (_upstream_)
  - Vastaus Varnishilta on palvelimen näkökulmasta ilmainen
- **ƒ(x) = y**
  - Pyynnöistä poistetaan varioivat tiedot

+++

## ƒ(x) + ? = ?

- HTTP on tilaton → **istunnot** (sessiot, eng. _session_)
- Istunto on **ulkoinen tila**
  - Liitetään asiakkaaseen istuntoevästeen avulla
- Istuntoeväste mukana pyynnössä → pyyntö välitetään upstreamille

+++

## Varnish: invalidointi

- Aika
- Cache-tagit
- URIBAN
- Koko välimuistin pudotus

---

## 5. PHP

- Tulkattu
- Shared nothing -malli

+++

## Koodin suorittaminen

1. PHP-koodi luetaan ja siitä muodostetaan **AST** (Abstract Syntax Tree)
2. AST:n perusteella muodostetaan suoritettavaa **opkoodia**
3. Opkoodi ajetaan Zend Engine -virtuaalikoneella

+++

## Shared nothing

- Koko suorituskonteksti rakennetaan **jokaista suorituskertaa varten erikseen**
- Jokaisesta suorituskerrasta vastaa **oma prosessinsa**, joka on varattuna koko suorituksen ajan

+++

## PHP:n suorituskyky

- OPcache
- PHP-FPM

+++

## OPcache

- Sisäänrakennettu mekanismi PHP 5.5.0:sta asti
- PHP-koodista muodostettu opkoodi tallennetaan välimuistiin
- Opkoodi ladataan välimuistista ja ajetaan suoraan

+++

## PHP-FPM

- PHP-prosessimanageri, joka viestii HTTP-palvelimen kanssa FastCGI-rajapinnan kautta
- PHP-prosessipooli → prosesseja ei luoda alusta asti uusiksi
- PHP-FPM:lle määritellään maksimiprosessimäärä
  - Jos prosessiraja tulee vastaan, pyyntö jää jonoon
  - Jos jonon maksimimitta tulee täyteen, pyyntö tiputetaan

+++

## Muisti

- Tietorakenteet ovat erittäin joustavia → vievät paljon muistia
- PHP-prosessille määritetään muistikatto (esim. 512 megatavua), jota se ei voi ylittää
  - Ylitysyritys kaataa prosessin (PHP-FPM hoitelee)
- PHP:n maksimimuistinkäyttö: P * M
  - P = prosessien maksimimäärä
  - M = prosessin muistikatto

+++

## Treston.com

- Palvelimen kokonaismuisti 6 Gt
- PHP:n muistikatto 1 Gt
- PHP-FPM:n maksimiprosessimäärä 50

+++

## Kun muisti loppuu

- OOM killer
- Swap

+++

## Kun muisti oikeasti loppuu

- Kaikki hidastuu
- Sovellus kaatuu

---

## 6. Drupal

- Drupal 7:n core: 63 000 riviä PHP-koodia
- Drupal 8:n core: 367 000 riviä PHP-koodia
- PHPMetrics: Karhuhelsinki.fi

+++

## Drupal 8 -sivusto on _massiivinen_ PHP-sovellus

+++

## Ongelmat

- PHP: suoritusmalli
- PHP: tietorakenteet
- Istunnot

+++

## Täysin uusi välimuistijärjestelmä

- Jokaisella asialla on välimuistikonteksti ja -tagit
- Eri konteksteihin luodaan eri instansseja
- Tagien perusteella invalidoidaan
- Välimuistia ei enää tyhjennetä, vaan se rakennetaan uudelleen

---

## 7. Kehitystyö

- Kehitysympäristö
- Drupalin asentaminen
- Konfiguraatio
- Räätälöidyt moduulit
- Teemaus

---

## Kehitysympäristö

1. Yksi VM, monta vhostia
2. Monta VM:ää
3. Docker

+++

## Docker

- Linux-konttien (eng. _container_) abstraktio
- Yksi "ohut" Linux-VM
- Mikropalveluarkkitehtuuri

+++

## Dockerin edut

- Vie vähemmän resursseja kuin "paksuihin" virtuaalikoneisiin perustuvat ratkaisut
- Ympäristöissä voi olla suurta varianssia
- Ympäristöt eivät vaikuta toisiinsa
- Ympäristöjen hallinta on nopeaa

+++

## Dockerin haitat

- Jaettujen hakemistojen suorituskyky _muualla kuin Linux-isännillä_
- Konfigurointi vaatii osaamista ja tietoa
- "Kiikkerä"

+++

## Ratkaisumahdollisuudet

- Odotetaan jaettujen hakemistojen suorituskyvyn paranemista
- Siirrytään käyttämään Linuxia kehittäjätyöasemilla
- Ajetaan palvelinohjelmistoja suoraan macOS:ssä ilman virtualisointia
- Tehdään kehitystyötä jollakin palvelimella

---

## Drupalin asentaminen

- Projekti: [Karhu Drupal 8 project](https://bitbucket.org/verkkojulkaisut/karhu-drupal-8-project/src/master/)
- Koodi: Composer-riippuvuudenhallintatyökalu
- Asennus: Drush, Drupal Console tai web-installeri
- Profiili: [Karhu Basic](https://bitbucket.org/verkkojulkaisut/karhu-basic-d8/src/master/)
- Teema: [Karhu Drupal 8 theme](https://bitbucket.org/verkkojulkaisut/karhu-drupal-8-theme/src/master/)

+++

## Automaatio

- Make-työkalu
- Oma Makefile
- Mahdollista käyttää muutamaa Docker-hallintatyökalua (Lando, Docksal)

+++

## Ylläpidettävät komponentit

- Projekti-template
- Asennusprofiili
- Teema
- Makefile
- Lando-konfiguraatio
- Docksal-konfiguraatio

+++

## Aika paljon ylläpidettävää, eikö?

---

## Drupalin konfiguraatio

- CMI
- Konfiguraatio on versiohallittua

+++

## Konfiguraation vienti palvelimelle

- `drush cex`, `git commit`, `git push`
- `git pull`, `drush cim`
- Automattisesti Bitbucket Pipelinesillä

+++

## Edut

- Konfiguraatio on valtaosa sivuston toiminnallista pohjaa → _on oltava_ versiohallittua
- Tuotantoonvienti onnistuu turvallisesti Pipelinesillä
- Konfiguraatio siirtyy automaattisesti kehittäjien välillä

+++

## Haitat

- Ei ole selvää rajaa, mikä on konfiguraatiota ja mikä ei
- Konfiguraatio ei välttämättä saa olla kaikissa ympäristöissä sama

---

## Räätälöidyt moduulit

- Moduulit ovat lähes täysin oliopohjaisia
- Metadataa YAML-tiedostoissa
- Metadataa kommentteina koodissa (annotaatiot)

+++

## Hyvää

- Modernin, oliopohjaisen PHP-sovelluskehitysmallin mukainen toteutus
- Selkeä hakemistohierarkia
- Laajennettavissa loputtomasti

+++

## Pahaa

- Vaatii _paljon_ syventymistä

+++

## Rumaa

- [Polkujen reititys](https://www.drupal.org/docs/8/api/routing-system)

---

## Teemaus

- Pohjateema
- Gulp-työnkulku
- Tyylit
- Kuvat
- JavaScript
- Sourcemapit

+++

## Pohjateema

- [Karhu Drupal 8 theme](https://bitbucket.org/verkkojulkaisut/karhu-drupal-8-theme/src/master/)
- Breakpointit
- Bower
- Faviconit
- Gulp-työnkulku

+++

## Breakpointit

- Määritellään `teema.breakpoints.yml`-tiedostossa
- Käytetään Gulp-työnkulussa SCSS-muuttujien luomiseen

+++

## Bower

- Vanha frontend-pakettimanageri
- Teema asettaa vain asetukset, ei lisää mitään riippuvuuksia

+++

## Faviconit

- Automaattisesti läjä favicon-koodia sivupohjiin
- Useimmilla sivustoilla kaikkia viitattuja tiedostoja ei ole olemassa

+++

## Gulp-työnkulku

- Koko asset-työnkulku yhdessä paikassa
- Vaatii _oikean_ Node.js- ja npm-version
- Vaatii _oikean_ globaalin Gulp-version

+++

## Tyylit

- Modulaarinen SCSS-rakennelma
- Mukana [Bourbon](https://www.bourbon.io/) ja [Neat](https://neat.bourbon.io/)
  - Näitä ei ole pakko käyttää
- Käännetään CSS:ksi Gulp-työnkulun mukaan
- Lopputuloksena yksi pakattu CSS-tiedosto

+++

## Kuvat

- Teeman kuvien _pitäisi_ mennä Gulp-työnkulun läpi
  - Tämä ei tällä hetkellä taida toimia?
- Tarkoituksena optimoida kuvat automaattisesti

+++

## JavaScript

- Gulp-työnkulku pakkaa JS-tiedostot automaattisesti (Uglify)
- Työnkulku kaatuu, jos JS:ssä on virhe

+++

## Sourcemapit

- Tehdään SCSS- ja JS-lähdetiedostoista
- Kertovat, mistä kohtaa lähdetiedostoa jokin sivulla oleva tyyli tai JS-koodipätkä löytyy

---

## Drupal-kehityksessä tarvittavia työkaluja

- Docker, Lando, Docksal, ddev
- Drush, Drupal Console
- Composer, npm, Bower, Yarn
- Gulp, Make
- Node.js, nvm, PHP
- Git, Bitbucket Pipelines

+++

## Teemmekö me oikeasti näin raskaan sarjan sovelluskehitystä?