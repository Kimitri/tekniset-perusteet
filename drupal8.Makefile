##
# Karhu Helsinki Drupal 8 project Makefile
#
# This Makefile is basically just a taskrunner that can be used to initialize a
# new Drupal 8 site.
#
# Author: Kimmo Tapala / Karhu Helsinki Oy
##

# Variables
PROJECT_DIR = ./tmp/project
PROJECT_REPO = git@bitbucket.org:verkkojulkaisut/karhu-drupal-8-project.git
THEME_REPO = git@bitbucket.org:verkkojulkaisut/karhu-drupal-8-theme.git
DOCKSAL_DIR = ./tmp/docksal
DOCKSAL_REPO = git@bitbucket.org:verkkojulkaisut/docksal-d8.git
LANDO_CONFIG = https://operations.karhuhelsinki.fi/static/lando_config/drupal8.yml
THEME_DIR = current/sites/default/themes
PROMPT = \x1b[32;01m[MAKE]»\x1b[0m 

# Mark all targets as PHONY
.PHONY: lando_drupal8 clone_drupal lando_setup lando_composer_install lando_clean docksal_drupal8 docksal_init clone_docksal docksal_composer_install docksal_init_site theme

# Set default target
.DEFAULT_GOAL := lando_drupal8
default: lando_drupal8;


# Installs a new Drupal 8 site using Lando
lando_drupal8: clone_drupal lando_setup lando_composer_install
	@echo "\n\n${PROMPT}Site created."
	@echo "${PROMPT}To create a custom theme, run: make THEME=<theme name> theme"


# Installs a new Drupal 8 site using Docksal
docksal_drupal8: clone_drupal docksal_init docksal_composer_install
	@echo "\n\n${PROMPT}Site created."
	@echo "${PROMPT}To create a custom theme, run: make THEME=<theme name> theme"


# Clone Drupal 8 template into the current directory
clone_drupal:
	@echo "${PROMPT}Cloning template project..."
	mkdir tmp
	git clone ${PROJECT_REPO} ${PROJECT_DIR}
	rm -rf ${PROJECT_DIR}/.git
	cp -r ${PROJECT_DIR}/. .
	-rm -rf tmp
	@echo "${PROMPT}Done."


# Clone Docksal config template into the current directory
clone_docksal:
	@echo "${PROMPT}Cloning Docksal config..."
	mkdir tmp
	git clone ${DOCKSAL_REPO} ${DOCKSAL_DIR}
	rm -rf ${DOCKSAL_DIR}/.git
	cp -r ${DOCKSAL_DIR}/.docksal .docksal
	-rm -rf tmp
	@echo "${PROMPT}Done."


# Set up Lando configuration and create containers
lando_setup:
	@echo "${PROMPT}Building Lando configuration..."
	curl ${LANDO_CONFIG} -o ./.lando.yml
	nano ./.lando.yml
	@echo "${PROMPT}Done."
	@echo "${PROMPT}Creating containers..."
	lando start
	@echo "${PROMPT}Done."


# Install dependencies using Composer
lando_composer_install:
	@echo "${PROMPT}Installing dependencies using Composer..."
	lando composer install
	@echo "${PROMPT}Done."
	@echo "${PROMPT}Removing VCS directories in children..."
	-lando composer cleanup-vcs-dirs
	@echo "${PROMPT}Done."


# Initialize a new environment using fin
docksal_init: clone_docksal
	@echo "${PROMPT}Creating containers..."
	fin init
	@echo "${PROMPT}Done."


# Install dependencies using Composer
docksal_composer_install:
	@echo "${PROMPT}Installing dependencies using Composer..."
	fin composer install
	@echo "${PROMPT}Done."
	@echo "${PROMPT}Removing VCS directories in children..."
	-fin composer cleanup-vcs-dirs
	@echo "${PROMPT}Done."


# Initialize a new D8 site using fin
docksal_init_site:
	@echo "${PROMPT}Initializing site..."
	fin init-site
	@echo "${PROMPT}Done."


# Create a custom theme
theme:
	@echo "${PROMPT}Building theme \"${THEME}\"..."
	mkdir -p ${THEME_DIR}
	git clone ${THEME_REPO} ${THEME_DIR}/${THEME}
	-rm -rf ${THEME_DIR}/${THEME}/.git
	mv ${THEME_DIR}/${THEME}/themename.breakpoints.yml ${THEME_DIR}/${THEME}/${THEME}.breakpoints.yml
	mv ${THEME_DIR}/${THEME}/themename.info.yml ${THEME_DIR}/${THEME}/${THEME}.info.yml
	mv ${THEME_DIR}/${THEME}/themename.libraries.yml ${THEME_DIR}/${THEME}/${THEME}.libraries.yml
	mv ${THEME_DIR}/${THEME}/themename.theme ${THEME_DIR}/${THEME}/${THEME}.theme
	mv ${THEME_DIR}/${THEME}/config/install/block.block.content.yml ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_content.yml
	mv ${THEME_DIR}/${THEME}/config/install/block.block.pagetitle.yml ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_pagetitle.yml
	mv ${THEME_DIR}/${THEME}/config/install/block.block.tabs.yml ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_tabs.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/gulpfile.js
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/package.json
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/bower.json
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/${THEME}.breakpoints.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/${THEME}.info.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/${THEME}.libraries.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/${THEME}.theme
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_content.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_pagetitle.yml
	sed -i.bak 's/{{THEMENAME}}/'${THEME}'/g' ${THEME_DIR}/${THEME}/config/install/block.block."${THEME}"_tabs.yml
	-rm ${THEME_DIR}/${THEME}/*.bak
	-rm ${THEME_DIR}/${THEME}/config/install/*.bak
	-cp current/core/modules/system/templates/maintenance-*.twig ${THEME_DIR}/${THEME}/templates
	@echo "\n\n${PROMPT}Theme \"${THEME}\" done."


# Destroys containers and removes all files except this Makefile
lando_clean:
	@echo "${PROMPT}Destroying containers..."
	-lando destroy -y
	@echo "${PROMPT}Done."
	@echo "${PROMPT}Removing files..."
	-find . ! -name 'Makefile' -type f -exec rm -f {} +
	-find . ! -name 'Makefile' -type d -exec rm -rf {} +
	@echo "${PROMPT}Done."


# Destroys containers and removes all files except this Makefile
docksal_clean:
	@echo "${PROMPT}Destroying containers..."
	-fin project remove
	@echo "${PROMPT}Done."
	@echo "${PROMPT}Removing files..."
	-find . ! -name 'Makefile' -type f -exec rm -f {} +
	-find . ! -name 'Makefile' -type d -exec rm -rf {} +
	@echo "${PROMPT}Done."